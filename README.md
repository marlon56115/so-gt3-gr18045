
## Ejecucion

Libreria estatica

```bash
$ gcc -c libstop.c -o libstop.o
$ ar rcs libstop.a libstop.o 
$ gcc main.c -o main  -L. -lstop
$ ./main 
```
Libreria dinamica

```bash
$ gcc libstop.c -c -fPIC
$ gcc libstop.o -shared -o libstop.so
$ export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH
$ gcc main.c -o main  -L. -lstop  
$ ./main 
```
Suma de 2 numeros en GAS assembly

```bash
$ gcc main.s -o main 
$ ./main 
```