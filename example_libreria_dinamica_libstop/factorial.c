int factorial(int number){
  int response = 1;
  for (int i = 1; i <= number; i++)
  {
    response *= i;
  }
  return response;
}